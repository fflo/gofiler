package gofiler

import (
	"testing"
)

// NewCandidate creates a new Candidate from a candidate string of the form:
// aby:abi+[(i:y,2)],dist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]
// or of the form
// norà:{nora+[(a:à,3)]}+ocr[(à:c,3)],voteWeight=0.000129274,levDistance=1
func TestCandidates(t *testing.T) {
	tests := []struct {
		test, ocr, mod, sug, dict string
		dist                      int
		weight                    float64
		iserr                     bool
	}{
		{"norà:{nora+[(a:à,3)]}+ocr[(à:c,3)],voteWeight=0.000129274,levDistance=1",
			"norc", "nora", "norà", "", 1, 0.000129274, false},
		{"aby:abi+[(i:y,2)],dist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]",
			"abc", "abi", "aby", "", 1, 0, false},
		{"aby:abi+[(i:y,2)],voteWeight=2.abx,dist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]",
			"abc", "abi", "aby", "", 1, 0, false},
		{"theil:{teil+[(t:th,0)]}+ocr[],voteWeight=0.63731,levDistance=0,dict=dict_xy",
			"theil", "teil", "theil", "dict_xy", 0, 0.63731, false},
		{"aby:abi+[(i:y,2)]ist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]", "", "", "", "", 0, 0, true},
		{"aby:abi+[(i:y,2)],dist=1(dict_modernhypotheticerror),ocr,[(y:c,2)]", "", "", "", "", 0, 0, true},
		{"abyabi+[(i:y,2)],dist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]", "", "", "", "", 0, 0, true},
		{"norà:{nora+[(aà,3)]}+ocr[(à:c,3)],voteWeight=0.000129274,levDistance=1", "", "", "", "", 0, 0, true},
		{"norà:{nora+[(a:à,3)]}+ocr[(à:c,3),voteWeight=0.000129274,levDistance=1", "", "", "", "", 0, 0, true},
		{"norà:{nora+[(a:à,3)]}+ocr[(à:c3)],voteWeight=0.000129274,levDistance=1", "", "", "", "", 0, 0, true},
	}
	for _, tc := range tests {
		t.Run(tc.test, func(t *testing.T) {
			c, err := NewCandidate(tc.test)
			if tc.iserr && err == nil {
				t.Fatal("expected error; got nil")
			}
			if !tc.iserr && err != nil {
				t.Fatalf("got err: %v", err)
			}
			if tc.iserr {
				return
			}
			if ocr, err := c.OCR(); ocr != tc.ocr || err != nil {
				if err != nil {
					t.Fatalf("got err: %v", err)
				}
				t.Fatalf("expected %s; got %s", tc.ocr, ocr)
			}
			if tc.mod != c.Modern {
				t.Fatalf("expected %s; got %s", tc.mod, c.Modern)
			}
			if tc.sug != c.Suggestion {
				t.Fatalf("expected %s; got %s", tc.sug, c.Suggestion)
			}
			if tc.dist != c.Distance {
				t.Fatalf("expected %d; got %d", tc.dist, c.Distance)
			}
			if tc.weight != c.Weight {
				t.Fatalf("expected %f; got %f", tc.weight, c.Weight)
			}
			if tc.dict != c.Dict {
				t.Fatalf("expected %s; got %s", tc.dict, c.Dict)
			}
		})
	}
}
func TestCandidatesString(t *testing.T) {
	tests := []string{
		"norà:{nora+[(a:à,3)]}+ocr[(à:c,3)],voteWeight=0.000129,levDistance=1,dict=",
	}
	for _, tc := range tests {
		t.Run(tc, func(t *testing.T) {
			c, err := NewCandidate(tc)
			if err != nil {
				t.Fatalf("got error: %v", err)
			}
			if got := c.String(); got != tc {
				t.Fatalf("expected %s; got %s", tc, got)
			}
			if got, _ := c.HistPatterns.ApplyRL(c.Suggestion); got != c.Modern {
				t.Fatalf("expected %s; got %s", c.Modern, got)
			}
		})
	}
}
