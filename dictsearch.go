package gofiler

// // DictSearch is used to run the runDictSearch executable.
// type DictSearch struct {
// 	stdout io.ReadCloser
// 	stdin  io.WriteCloser
// 	cmd    *exec.Cmd
// }

// // NewDictSearch returns a new DictSearch instance.
// // This functions starts the runDictSearch process
// // and connects to its stdin and stdout.
// func NewDictSearch(lang string, b Backend) (*DictSearch, error) {
// 	cmd := exec.Command(b.DictSearch(), "--config", b.LanguageConfiguration(lang))
// 	stdin, err := cmd.StdinPipe()
// 	if err != nil {
// 		return nil, err
// 	}
// 	stdout, err := cmd.StdoutPipe()
// 	if err != nil {
// 		return nil, err
// 	}
// 	if err := cmd.Start(); err != nil {
// 		return nil, err
// 	}
// 	return &DictSearch{stdin: stdin, stdout: stdout, cmd: cmd}, nil
// }

// // Query runs a query on the runDictSearch process and returns its results.
// func (d *DictSearch) Query(q string) (*Candidates, error) {
// 	_, err := io.WriteString(d.stdin, q+"\n")
// 	if err != nil {
// 		return nil, err
// 	}
// 	return readCandidates(q, d.stdout)
// }

// // Close closes the runDictSearch process.
// func (d DictSearch) Close() error {
// 	_ = d.stdin.Close()
// 	_ = d.stdout.Close()
// 	return d.cmd.Wait()
// }

// const (
// 	none = "--NONE--"
// 	end  = "--END--"
// )

// func readCandidates(base string, r io.Reader) (*Candidates, error) {
// 	cs := Candidates{OCR: base, Candidates: nil}
// 	scanner := bufio.NewScanner(r)
// 	for scanner.Scan() {
// 		text := scanner.Text()
// 		if text == none {
// 			break
// 		}
// 		if text == end {
// 			break
// 		}
// 		c, err := NewCandidate(text)
// 		if err != nil {
// 			return nil, err
// 		}
// 		cs.Candidates = append(cs.Candidates, c)
// 	}
// 	if err := scanner.Err(); err != nil {
// 		return nil, err
// 	}
// 	return &cs, nil
// }
