package gofiler

import (
	"os"
	"path/filepath"
	"sort"
	"testing"
)

const (
	testLangDir = "testdata/backend"
)

func TestLocalLanguages(t *testing.T) {
	languages := []string{"a", "b", "c"}
	locallangs, err := LocalLanguages(testLangDir)
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
	sort.Strings(locallangs)
	if len(languages) != len(locallangs) {
		t.Fatalf("expected %v; got %v", languages, locallangs)
	}
	for i := range locallangs {
		if languages[i] != locallangs[i] {
			t.Fatalf("expceted %s; got %s", languages[i], locallangs[i])
		}
	}
}

func TestLocalLanguageConfigurations(t *testing.T) {
	tests := []struct {
		lang, path string
		exists     bool
	}{
		{"a", filepath.Join(testLangDir, "a.ini"), true},
		{"b", filepath.Join(testLangDir, "b.ini"), true},
		{"c", filepath.Join(testLangDir, "c.ini"), true},
		{"d", "", false},
		{"e", "", false},
	}
	for _, tc := range tests {
		t.Run(tc.lang, func(t *testing.T) {
			path, ok := LocalLanguageConfiguration(testLangDir, tc.lang)
			if ok != tc.exists {
				t.Fatalf("expected %t; got %t", tc.exists, ok)
			}
			if path != tc.path {
				t.Fatalf("expected %s; got %s", tc.path, path)
			}
		})
	}
}

func TestLocalProfilerProfile(t *testing.T) {
	tfile, _ := os.Open("testdata/theil.txt")
	defer func() { _ = tfile.Close() }()
	profile, err := readProfileFromTextFile(tfile)
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
	tests := []struct {
		word string
		len  int
	}{
		{"theil", 119},
		{"thurm", 1},
		{"vnnd", 102},
		{"NOPE", 0},
	}
	for _, tc := range tests {
		t.Run(tc.word, func(t *testing.T) {
			cs := profile[tc.word]
			if cs == nil && tc.len != 0 {
				t.Fatalf("missing candidates for %s", tc.word)
			}
			// skip non existing
			if cs == nil {
				return
			}
			if got := cs.OCR; got != tc.word {
				t.Fatalf("expected %s; got %s", tc.word, got)
			}
			if got := len(cs.Candidates); got != tc.len {
				t.Fatalf("expected %d; got %d", tc.len, got)
			}
		})
	}
}
