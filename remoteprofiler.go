package gofiler

import (
	"encoding/xml"
	"io"

	"github.com/pkg/errors"
)

type remoteProfiler struct {
	url string
}

// NewRemoteProfiler returns a new profiler instance,
// that connects to a profiler-web-service.
func NewRemoteProfiler(url string) Profiler {
	return &remoteProfiler{url}
}

//    "http://langprofiler.informatik.uni-leipzig.de/axis2/services/"
// ////////////////////////////////////////////////////////////////////////////////
// std::vector<std::string> RemoteProfiler::parse_languages(
//     const std::string& data) {
// 	pugi::xml_document xml;
// 	const auto res = xml.load_buffer(data.data(), data.size());
// 	if (not res) {
// 		THROW(Error, "could not get languages: ", res.description());
// 	}
// 	const auto pre = get_namespace_prefix(xml.document_element());
// 	auto nodes = xml.document_element().select_nodes(
// 	    (".//" + pre + "configurations").data());
// 	std::vector<std::string> languages;
// 	for (const auto& node : nodes) {
// 		languages.push_back(node.node().child_value());
// 	}
// 	return languages;
// }

// const auto path = boost::filesystem::path(url_) / "getConfigurations";
// char EBUF[CURL_ERROR_SIZE + 1];
// buffer_.clear();
// auto curl = new_curl();
// // curl_easy_setopt(curl.get(), CURLOPT_PORT, port);
// curl_easy_setopt(curl.get(), CURLOPT_URL, path.string().data());
// curl_easy_setopt(curl.get(), CURLOPT_ERRORBUFFER, EBUF);
// // curl_easy_setopt(curl.get(), CURLOPT_COOKIEFILE,
// // cookiefile.string().data());
// // curl_easy_setopt(curl.get(), CURLOPT_COOKIEJAR,
// // cookiefile.string().data());
// curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, &write);
// curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, this);
// // curl_easy_setopt(curl.get(), CURLOPT_READFUNCTION, &read);
// // curl_easy_setopt(curl.get(), CURLOPT_READDATA, this);
// if (curl_easy_perform(curl.get()) != CURLE_OK)
// 	THROW(Error, "curl_easy_perfrom(): ", EBUF);
// long http_code = 0;
// curl_easy_getinfo(curl.get(), CURLINFO_RESPONSE_CODE, &http_code);
// if (http_code != 200) THROW(Error, "invalid return code: ", http_code);
// CROW_LOG_DEBUG << "(RemoteProfiler) languages: " << buffer_;
// return parse_languages(buffer_);

func (*remoteProfiler) Profile(io.Reader) (Profile, error) {
	return nil, errors.New("not implemented: Profile()")
}

type docXMLCandidates struct {
	Candidates []docXMLCandidate `xml:"page>token"`
}

type docXMLCandidate struct {
	OCR         string   `xml:"wOCR"`
	Normal      bool     `xml:"isNormal,attr"`
	Suggestions []string `xml:"cand"`
}

func readProfileFromDocXMLFile(r io.Reader) (Profile, error) {
	cs := &docXMLCandidates{}
	if err := xml.NewDecoder(r).Decode(cs); err != nil {
		return nil, errors.Wrapf(err, "could not decode DocXML")
	}
	profile := make(Profile)
	for _, c := range cs.Candidates {
		if _, ok := profile[c.OCR]; ok {
			continue
		}
		if !c.Normal {
			continue
		}
		if len(c.Suggestions) == 0 {
			continue
		}
		candidates := &Candidates{
			OCR:        c.OCR,
			Candidates: make([]Candidate, len(c.Suggestions)),
		}
		for i, s := range c.Suggestions {
			cand, err := NewCandidate(s)
			if err != nil {
				return nil, errors.Wrapf(err, "could not read candidate")
			}
			candidates.Candidates[i] = cand
		}
		profile[c.OCR] = candidates
	}
	return profile, nil
}
