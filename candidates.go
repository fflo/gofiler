package gofiler

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	distExpr   = regexp.MustCompile(`,dist=(\d*)`)
	levExpr    = regexp.MustCompile(`,levDistance=(\d*)`)
	weightExpr = regexp.MustCompile(`,voteWeight=([^,]*)`)
	histExpr   = regexp.MustCompile(`\+(\[(\([^:]*:[^,]*,\d+\))*\])`)
	ocrExpr    = regexp.MustCompile(`[\+,]ocr=?(\[(\([^:]*:[^,]*,\d+\))*\])`)
	modExpr    = regexp.MustCompile(`:{?(.*?)\+`)
	dictExpr   = regexp.MustCompile(`dict=([^,]+)`)
)

// Profile is a map of the OCR tokens and their candidates.
// Each OCR token points to its according candidates.
type Profile map[string]*Candidates

// Candidates is a list of suggested candidates for a given OCR base word.
type Candidates struct {
	OCR        string
	Candidates []Candidate
}

// Candidate represents a correction candidate with a specific
// historical rewrite rule and a list of different possible ocr error patterns.
type Candidate struct {
	Suggestion, Modern, Dict  string
	HistPatterns, OCRPatterns PosPatterns
	Distance                  int
	Weight                    float64
}

// String prints the string representation of the candiates.
// Uses the profiler's output format:
// norà:{nora+[(a:à,3)]}+ocr[(à:c,3)],voteWeight=0.000129274,levDistance=1
func (c Candidate) String() string {
	return fmt.Sprintf("%s:{%s+%s}+ocr%s,voteWeight=%f,levDistance=%d,dict=%s",
		c.Suggestion, c.Modern, c.HistPatterns, c.OCRPatterns, c.Weight, c.Distance, c.Dict)
}

// OCR calculates the original OCR string based on the OCR patterns of the candidate.
func (c Candidate) OCR() (string, error) {
	return c.OCRPatterns.ApplyLR(c.Suggestion)
}

// PosPatterns is an array of PosPatterns.
type PosPatterns []PosPattern

func (ps PosPatterns) String() string {
	s := "["
	for _, p := range ps {
		s += "(" + p.String() + ")"
	}
	return s + "]"
}

// ApplyLR applies all patterns from left to right on a string.
func (ps PosPatterns) ApplyLR(str string) (string, error) {
	for _, p := range ps {
		tmp, err := p.ApplyLR(str)
		if err != nil {
			return "", err
		}
		str = tmp
	}
	return str, nil
}

// ApplyRL applies all patterns from right to left on a string.
func (ps PosPatterns) ApplyRL(str string) (string, error) {
	for _, p := range ps {
		tmp, err := p.ApplyRL(str)
		if err != nil {
			return "", err
		}
		str = tmp
	}
	return str, nil
}

// PosPattern holds the position of a pattern and it's left and right side.
// PosPattern uses unicode points to ease the application of patterns onto strings.
type PosPattern struct {
	Left, Right string
	Pos         int
}

// ApplyLR matches the left side of a pattern and replaces
// it with its right side.
func (p PosPattern) ApplyLR(str string) (string, error) {
	return apply(p.Left, p.Right, str, p.Pos)
}

// ApplyRL matches the right side of a pattern and replaces
// it with its left side.
func (p PosPattern) ApplyRL(str string) (string, error) {
	return apply(p.Right, p.Left, str, p.Pos)
}

func (p PosPattern) String() string {
	return fmt.Sprintf("%s:%s,%d", p.Left, p.Right, p.Pos)
}

func parsePosPatterns(s string) (PosPatterns, error) {
	var ps PosPatterns
	for i := strings.Index(s, "("); i != -1; i = strings.Index(s, "(") {
		j := strings.Index(s, ")")
		if j == -1 {
			return nil, fmt.Errorf("invalid patterns expression: %s", s)
		}
		p, err := parsePosPattern(s[i+1 : j])
		if err != nil {
			return nil, err
		}
		ps = append(ps, p)
		s = s[j+1:]
	}
	return ps, nil
}

func parsePosPattern(s string) (PosPattern, error) {
	a := strings.Index(s, ":")
	b := strings.Index(s, ",")
	if a == -1 || b == -1 {
		return PosPattern{}, fmt.Errorf("invalid pattern expression: %s", s)
	}
	p, err := strconv.Atoi(s[b+1:])
	if err != nil {
		return PosPattern{}, fmt.Errorf("invalid pattern expression: %s", s)
	}
	return PosPattern{
		Left:  s[0:a],
		Right: s[a+1 : b],
		Pos:   p,
	}, nil
}

func apply(l, r, s string, p int) (string, error) {
	if len(s) == p && len(l) == 0 {
		return s + r, nil
	}
	var n int
	for i := range s {
		if n < p {
			n++
			continue
		}
		if !strings.HasPrefix(s[i:], l) {
			return "", fmt.Errorf("%d %d: b cannot apply (%s:%s,%d) to %s", i, n, l, r, p, s)
		}
		return s[0:i] + r + s[i+len(l):], nil
	}
	return "", fmt.Errorf("c cannot apply (%s:%s,%d) to %s", l, r, p, s)
}

// NewCandidate creates a new Candidate from a candidate string of the form:
// aby:abi+[(i:y,2)],dist=1(dict_modernhypotheticerror),ocr=[(y:c,2)]
// or of the form
// norà:{nora+[(a:à,3)]}+ocr[(à:c,3)],voteWeight=0.000129274,levDistance=1
func NewCandidate(str string) (Candidate, error) {
	suggestion, err := parseSuggestion(str)
	if err != nil {
		return Candidate{}, err
	}
	modern, err := parseModern(str)
	if err != nil {
		return Candidate{}, err
	}
	histp, err := parseHistPosPatterns(str)
	if err != nil {
		return Candidate{}, err
	}
	ocrp, err := parseOCRPosPatterns(str)
	if err != nil {
		return Candidate{}, err
	}
	dist, err := parseLevDistance(str)
	if err != nil {
		return Candidate{}, err
	}
	weight := parseWeight(str)
	dict := parseDict(str)
	return Candidate{
		Suggestion:   suggestion,
		Modern:       modern,
		Distance:     dist,
		HistPatterns: histp,
		OCRPatterns:  ocrp,
		Dict:         dict,
		Weight:       weight,
	}, nil
}

func parseSuggestion(str string) (string, error) {
	i := strings.Index(str, ":")
	if i < 0 {
		return "", fmt.Errorf("missing suggestion: %s", str)
	}
	return str[0:i], nil
}

func parseModern(str string) (string, error) {
	matches := modExpr.FindStringSubmatch(str)
	if len(matches) == 0 {
		return "", fmt.Errorf("missing modern word: %s", str)
	}
	return matches[1], nil
}

func parseWeight(str string) float64 {
	matches := weightExpr.FindStringSubmatch(str)
	if len(matches) == 0 {
		return 0
	}
	d, err := strconv.ParseFloat(matches[1], 64)
	if err != nil {
		return 0
	}
	return d
}

func parseLevDistance(str string) (int, error) {
	matches := distExpr.FindStringSubmatch(str)
	if len(matches) == 0 {
		matches = levExpr.FindStringSubmatch(str)
	}
	if len(matches) == 0 {
		return 0, fmt.Errorf("invalid candidate expr (distance): %s", str)
	}
	d, err := strconv.Atoi(matches[1])
	if err != nil {
		return 0, err
	}
	return d, nil
}

func parseHistPosPatterns(str string) (PosPatterns, error) {
	matches := histExpr.FindStringSubmatch(str)
	if matches == nil {
		return nil, fmt.Errorf("invalid candidate expr (hist pattern): %s", str)
	}
	return parsePosPatterns(matches[1])
}

func parseOCRPosPatterns(str string) (PosPatterns, error) {
	matches := ocrExpr.FindStringSubmatch(str)
	if matches == nil {
		return nil, fmt.Errorf("invalid candidate expr (ocr pattern): %s", str)
	}
	return parsePosPatterns(matches[1])
}

func parseDict(str string) string {
	matches := dictExpr.FindStringSubmatch(str)
	if len(matches) == 2 {
		return matches[1]
	}
	return ""
}
