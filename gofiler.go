package gofiler

import "io"

// Profiler defines the interface for profiling documents
type Profiler interface {
	Profile(io.Reader) (Profile, error)
}

// Configuration defines the interface for language configurations
type Configuration interface {
	Languages() ([]string, error)
	Configuration(string) (string, bool)
}
