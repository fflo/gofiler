package gofiler

import (
	"fmt"
	"os"
	"testing"
)

func TestProfileProgress(t *testing.T) {
	tests := []struct {
		mi, ni, mt, nt int
		percent        float64
	}{
		{1, 1, 10, 0, 0},
		{1, 1, 10, 1, 0.1},
		{1, 1, 10, 2, 0.2},
		{1, 1, 10, 7, 0.7},
		{1, 1, 10, 10, 1},
		{2, 1, 10, 0, 0},
		{2, 1, 10, 1, 0.05},
		{2, 1, 10, 2, 0.1},
		{2, 1, 10, 6, 0.3},
		{2, 1, 10, 10, 0.5},
		{2, 2, 10, 0, 0.5},
		{2, 2, 10, 1, 0.55},
		{2, 2, 10, 2, 0.6},
		{2, 2, 10, 6, 0.8},
		{2, 2, 10, 10, 1},
	}
	for _, tc := range tests {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			progress := &ProfileProgress{
				Tokens:        tc.nt,
				MaxTokens:     tc.mt,
				MaxIterations: tc.mi,
				Iterations:    tc.ni,
			}
			if got := progress.Percent(); got != tc.percent {
				t.Fatalf("expected %f; got %f", tc.percent, got)
			}
		})
	}
}

func TestProfilerProgressUpdate(t *testing.T) {
	tfile, _ := os.Open("testdata/theil-stderr.txt")
	p := &ProfileProgress{}
	defer func() { _ = tfile.Close() }()
	readProgress(tfile, func(l []byte) {
		p.Update(l)
	})
	if p.MaxIterations != 4 {
		t.Fatalf("expected %d; got %d", 4, p.MaxIterations)
	}
	if p.MaxTokens != 29000 {
		t.Fatalf("expected %d; got %d", 101000, p.MaxTokens)
	}
	if p.Tokens > 29000 {
		t.Fatalf("expected <= %d; got %d", 29000, p.Tokens)
	}
	if p.Iterations > 4 {
		t.Fatalf("expected <= %d; got %d", 4, p.Iterations)
	}
	if p.Iterations < 1 {
		t.Fatalf("expected <= %d; got %d", 1, p.Iterations)
	}
}
