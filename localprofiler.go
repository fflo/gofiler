package gofiler

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

// LocalLanguages returns all available languages in the language directory.
// It iterates over all files in the language directory directory and gathers
// all configuration files.
// Each file of the form L.ini in the directory is assumed to be
// a configuration file for the language L.
func LocalLanguages(langDir string) ([]string, error) {
	files, err := ioutil.ReadDir(langDir)
	if err != nil {
		return nil, errors.Wrapf(err, "could not read language dir: %s", langDir)
	}
	var langs []string
	for _, info := range files {
		if info.IsDir() {
			continue
		}
		name := info.Name()
		if dot := strings.LastIndex(name, "."); dot > 0 {
			if name[dot:] == ".ini" {
				langs = append(langs, name[0:dot])
			}
		}
	}
	return langs, nil
}

// LocalLanguageConfiguration returns the path of a language configuration file and
// whether the given language exists.
func LocalLanguageConfiguration(langDir, lang string) (string, bool) {
	path := filepath.Join(langDir, lang+".ini")
	stat, err := os.Stat(path)
	if err != nil {
		return "", false
	}
	return path, !stat.IsDir()
}

// NewLocalProfiler constructs a local profiler
func NewLocalProfiler(d, l, f string, c func([]byte)) (Profiler, error) {
	format, err := profilerInputFormat(f)
	if err != nil {
		return nil, err
	}
	return &localProfiler{
		cmdDir:        d,
		callback:      c,
		format:        format,
		configuration: l,
	}, nil
}

type localProfiler struct {
	callback      func([]byte)
	lock          sync.Mutex
	profile       Profile
	err           error
	cmdDir        string
	configuration string
	format        string
}

func (p *localProfiler) Profile(r io.Reader) (Profile, error) {
	args := p.args()
	cmd := exec.Command(args[0], args[1:]...)
	p.err = nil
	p.profile = make(Profile)

	if err := p.readProgress(cmd); err != nil {
		return nil, errors.Wrapf(err, "%s: could not read profiler progress", args)
	}
	if err := p.readProfile(cmd); err != nil {
		return nil, errors.Wrapf(err, "%s: could not read profiler output", args)
	}
	if err := p.writeContent(cmd, r); err != nil {
		return nil, errors.Wrapf(err, "%s: could not write content to profiler", args)
	}
	if err := cmd.Start(); err != nil {
		return nil, errors.Wrapf(err, "%s: could not execute command", args)
	}
	if err := cmd.Wait(); err != nil {
		return nil, errors.Wrapf(err, "%s: could not execute command", args)
	}
	return p.profile, p.err
}

func (p *localProfiler) readProgress(cmd *exec.Cmd) error {
	if p.callback == nil {
		return nil
	}
	pipe, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	go func() {
		defer func() { _ = pipe.Close() }()
		p.pushError(readProgress(pipe, p.callback))
	}()
	return nil
}

func (p *localProfiler) readProfile(cmd *exec.Cmd) error {
	pipe, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	go func() {
		defer func() { _ = pipe.Close() }()
		profile, err := readProfileFromTextFile(pipe)
		p.profile = profile
		p.pushError(err)
	}()
	return nil
}

func (p *localProfiler) writeContent(cmd *exec.Cmd, r io.Reader) error {
	pipe, err := cmd.StdinPipe()
	if err != nil {
		return err
	}
	go func() {
		defer func() { p.pushError(pipe.Close()) }()
		_, err := io.Copy(pipe, r)
		p.pushError(err)
	}()
	return nil
}

func (p *localProfiler) args() []string {
	return []string{
		filepath.Join(p.cmdDir, "profiler"),
		"--config", p.configuration,
		"--sourceFile", "/dev/stdin",
		"--sourceFormat", p.format,
		"--adaptive",
		"--simpleOutput",
	}
}

func (p *localProfiler) pushError(err error) {
	if err == nil {
		return
	}
	p.lock.Lock()
	defer func() { p.lock.Unlock() }()
	if p.err == nil {
		p.err = nil
	}
}

func readProfileFromTextFile(r io.Reader) (Profile, error) {
	scanner := bufio.NewScanner(r)
	profile := make(Profile)
	tokens := make(map[string]bool)
	var ocr string
	for scanner.Scan() {
		line := scanner.Bytes()
		if len(line) == 0 {
			continue
		}
		if line[0] == '@' {
			token := string(line[1:])
			if tokens[token] {
				ocr = ""
			} else {
				tokens[token] = true
				ocr = token
			}
		} else if ocr != "" {
			c, err := NewCandidate(string(line))
			if err != nil {
				return nil, errors.Wrapf(err, "invalid candidate in profiler output")
			}
			if _, ok := profile[ocr]; !ok {
				profile[ocr] = &Candidates{OCR: ocr}
			}
			profile[ocr].Candidates = append(profile[ocr].Candidates, c)
		}
	}
	if scanner.Err() != nil {
		return nil, errors.Wrapf(scanner.Err(), "could not read profiler's output")
	}
	return profile, nil
}

func readProgress(r io.Reader, f func([]byte)) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		f(scanner.Bytes())
	}
	if scanner.Err() != nil {
		return errors.Wrapf(scanner.Err(), "could not read profiler's progress")
	}
	return nil
}

func profilerInputFormat(f string) (string, error) {
	switch strings.ToLower(f) {
	case "xml", "docxml", "application/xml":
		return "DocXML", nil
	case "txt", "text/plain":
		return "TXT", nil
	default:
		return "", errors.Errorf("invalid input format: %s", f)
	}
}
