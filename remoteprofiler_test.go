package gofiler

import (
	"os"
	"testing"
)

func TestRemoteProfilerProfile(t *testing.T) {
	tfile, _ := os.Open("testdata/theil.doc.xml")
	defer func() { _ = tfile.Close() }()
	profile, err := readProfileFromDocXMLFile(tfile)
	if err != nil {
		t.Fatalf("got error: %s", err)
	}
	tests := []struct {
		word string
		len  int
	}{
		{"Theil", 124},
		{"Thurm", 1},
		{"theil", 124},
		{"thurm", 1},
		{"NOPE", 0},
	}
	for _, tc := range tests {
		t.Run(tc.word, func(t *testing.T) {
			cs := profile[tc.word]
			if cs == nil && tc.len != 0 {
				t.Fatalf("missing candidates for %s", tc.word)
			}
			// skip non existing
			if cs == nil {
				return
			}
			if got := cs.OCR; got != tc.word {
				t.Fatalf("expected %s; got %s", tc.word, got)
			}
			if got := len(cs.Candidates); got != tc.len {
				t.Fatalf("expected %d; got %d", tc.len, got)
			}
		})
	}
}
