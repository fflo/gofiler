package gofiler

import (
	"regexp"
	"strconv"
)

// ProfileProgress captures the progress of the profiler.
type ProfileProgress struct {
	MaxTokens, Tokens, MaxIterations, Iterations int
}

// Percent returns the current progress in percent.
func (p *ProfileProgress) Percent() float64 {
	return (float64(p.Tokens) + (float64(p.MaxTokens) * (float64(p.Iterations - 1)))) /
		(float64(p.MaxTokens) * float64(p.MaxIterations))
}

// Update updates the progress based on a stderr line of the profiler.
// It returns true if the progress has changed.
func (p *ProfileProgress) Update(line []byte) bool {
	if m := miterations.FindSubmatch(line); m != nil {
		n, _ := strconv.Atoi(string(m[1]))
		p.MaxIterations = n
		return false // no update -- just max iterations
	}
	if m := niterations.FindSubmatch(line); m != nil {
		n, _ := strconv.Atoi(string(m[1]))
		p.Iterations = n
		if p.Iterations > 1 { // adjust max number of tokens
			p.MaxTokens = p.Tokens
		}
		p.Tokens = 0
		return true
	}
	if m := tokens.FindSubmatch(line); m != nil {
		n, _ := strconv.Atoi(string(m[1]))
		p.Tokens = n * 1000
		if p.MaxTokens == 0 {
			max, _ := strconv.Atoi(string(m[2]))
			p.MaxTokens = max * 1000
		}
		return true
	}
	return false
}

var (
	miterations = regexp.MustCompile(`number\s+of\s+iterations:\s+(\d+)`)
	niterations = regexp.MustCompile(`Iteration\s+(\d+)`)
	tokens      = regexp.MustCompile(`(\d+)k/(\d+)k\s+tokens\s+processed\s+in`)
)
